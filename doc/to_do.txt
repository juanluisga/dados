- Implementar tirada completa para un jugador, implementando dos estructuras
(¿listas? STL): una referida a la cubeta (dados que van a lanzarse o se 
descartan) y otra para los dados que contabilizan en la tirada. 

- Realizar la validación de la tirada, de modo que solo acepte la selección de 
dados permitida (teniendo en cuenta los ases como comodines)

- Mostrar la puntuación de la tirada.

- Implementar jugada para segundo jugador (humano).

- Segundo jugador (máquina)