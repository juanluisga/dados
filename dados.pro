CONFIG += c++11

QT += widgets

HEADERS += \
    src/dado.h \
    src/jugador.h \
    src/pantalla.h \
    src/partida.h \
    src/puntuacion.h \
    src/tipo_jugador.h \
    src/tipos.h

SOURCES += \
    src/dado.cpp \
    src/jugador.cpp \
    src/main.cpp \
    src/pantalla.cpp \
    src/partida.cpp

OTHER_FILES += \
    doc/develop_log.txt \
    doc/flujo.dia \
    doc/modelo.dia \
    doc/to_do.txt \
    contributors.txt \
    dados.txt
