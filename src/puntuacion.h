#ifndef PUNTUACION_H
#define PUNTUACION_H

#include "tipos.h"

struct Ronda{
	USINT puntosNegro;
	USINT puntosRojo;
	USINT puntosJota;
	USINT puntosDama;
	USINT puntosRey;
	USINT puntosAs;
	USINT totalRonda;
};

struct Puntuacion{
	Ronda rondaLibre;
	Ronda rondaObligada;
	USINT totalPuntos;
};

#endif // PUNTUACION_H