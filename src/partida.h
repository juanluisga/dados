#ifndef PARTIDA_H
#define PARTIDA_H

#include <vector>
#include "tipos.h"
#include "jugador.h"
#include "tipo_jugador.h"


class Partida{
	
	private:
		
		const USINT TOTAL_RONDAS = 2;
		const USINT TOTAL_TURNOS_RONDA = 6;
		const USINT TOTAL_TIRADAS = 3;
	
		vector<Jugador * > jugadores;
		
		
	public:
		Partida();
		Partida(USINT);
		~Partida();
		USINT totalJugadores();
		void inscribirJugador(Jugador * );

};

#endif // PARTIDA_H
