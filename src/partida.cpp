#include "partida.h"
#include "tipos.h"
#include "tipo_jugador.h"

// CONSTRUCTORES
Partida::Partida(){
}

Partida::Partida(USINT numJugadores){
	//jugadores.resize(numJugadores);
}

// DESTRUCTOR
Partida::~Partida(){
	for (USINT i = 0; i < jugadores.size(); i++){
		delete jugadores.at(i)->getPuntuacion();
		jugadores.at(i)->setPuntuacion(nullptr);
		jugadores.at(i) = nullptr;
		//delete jugadores.at(i);
	}
	jugadores.clear();
}

// MÉTODOS
USINT Partida::totalJugadores(){
	return jugadores.size();
}

void Partida::inscribirJugador(	Jugador * jugador){
	jugador->setPuntuacion(new Puntuacion());
	jugadores.push_back(jugador);
	
}