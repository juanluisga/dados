#ifndef PANTALLA_H
#define PANTALLA_H

#include "string"
#include "tipos.h"

using namespace std;

class Pantalla{
	public:
		// CONSTRUCTORES
		Pantalla();
		
		// DESTRUCTOR
		~Pantalla();

		// GETTERS & SETTERS

		
		// MÉTODOS
        void presentacion(void);
		void menuPrincipal(void);
		
    private:
		// CONSTANTES

		
		// PROPIEDADES

};

#endif // PANTALLA_H
