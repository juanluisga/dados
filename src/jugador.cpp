#include "jugador.h"
#include "tipo_jugador.h"
#include <string>

// CONSTRUCTORES
Jugador::Jugador(){
}

Jugador::Jugador(string _nombre, TipoJugador _tipo): 	
	nombre(_nombre), 
	tipo(_tipo)
{
	this->puntuacion = nullptr;
}

// DESTRUCTOR
Jugador::~Jugador()
{
}

// SETTERS & GETTERS
void Jugador::setNombre(string nombre){
	this->nombre = nombre;
}

string Jugador::getNombre(){
	return this->nombre;
}

void Jugador::setTipo(TipoJugador tipo){
	this->tipo = tipo;
}

TipoJugador Jugador::getTipo(void){
	return this->tipo;
}

void Jugador::setPuntuacion(Puntuacion * puntuacion){
	this->puntuacion = puntuacion;
}

Puntuacion * Jugador::getPuntuacion(){
	return this->puntuacion;
}
