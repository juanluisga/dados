#include "dado.h"
#include <cstdlib>


Dado::Dado(){
    //ctor
}

Dado::~Dado(){
    //dtor
}

Cara Dado::getValor(void){
    return valor;
}

char Dado::getValorLiteral(void){
	char literal;
	switch( this->valor ){
		case NEGRO:
			literal = 'N';
			break;
		case ROJO:
			literal = 'R';
			break;
		case JOTA:
			literal = 'J';
			break;
		case DAMA:
			literal = 'Q';
			break;
		case REY:
			literal = 'K';
			break;
		case AS:
			literal = 'A';
			break;
	}
	return literal;
}

void Dado::lanzar(void){
    valor = Cara((1 + rand() % 6));
}
