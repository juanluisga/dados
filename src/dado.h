#ifndef DADO_H
#define DADO_H


enum Cara {
    NEGRO = 1,
    ROJO = 2,
    JOTA = 3,
    DAMA = 4,
    REY = 5,
    AS = 6
};

class Dado{
    public:
        Dado();
        virtual ~Dado();
        void lanzar(void);
        Cara getValor(void);
		char getValorLiteral(void);

    protected:

    private:
        Cara valor;
};

#endif // DADO_H
