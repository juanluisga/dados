#include <string>
#include "tipos.h"
#include "tipo_jugador.h"
#include "puntuacion.h"

using namespace std;

#ifndef JUGADOR_H
#define JUGADOR_H

class Jugador{
	
	public:
		Jugador();
		Jugador(string, TipoJugador);
		~Jugador();
		void setNombre(string);
		string getNombre(void);
		void setTipo(TipoJugador);
		TipoJugador getTipo(void);
		void setPuntuacion(Puntuacion * );
		Puntuacion * getPuntuacion(void);

	private:
		string nombre;
		TipoJugador tipo;
		Puntuacion * puntuacion;
};

#endif // JUGADOR_H
